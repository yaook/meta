# This funny little script will go through all MRs opened by the yaook-bot, approve them, and attempt to merge them if the pipeline was ok.

import gitlab
import requests
import time
import os

gl = gitlab.Gitlab("https://gitlab.com")
# author_id is the renovate bot
group = gl.groups.get("yaook")
mrs = group.mergerequests.list(all=True, author_id=7503825, state="opened")
for mr in mrs:
  print(f"processing MR {mr.title}")
  mrps = requests.get(f"https://gitlab.com/api/v4/projects/{mr.target_project_id}/merge_requests/{mr.iid}/pipelines").json()
  mrps.sort(key=lambda x: x["id"], reverse=True)
  if mrps and mrps[0]['status'] == 'success':
    print(" .. merging!")
    try:
      requests.post(f"https://gitlab.com/api/v4/projects/{mr.target_project_id}/merge_requests/{mr.iid}/approve", headers={"Authorization": f"Bearer {os.environ['GITLAB_TOKEN']}"}).raise_for_status()
    except requests.exceptions.HTTPError as exc:
      if exc.response.status_code != 401:
        raise
      print("    failed to approve, trying to merge anyway.")

    try:
      requests.put(f"https://gitlab.com/api/v4/projects/{mr.target_project_id}/merge_requests/{mr.iid}/merge?merge_when_pipeline_succeeds=true", headers={"Authorization": f"Bearer {os.environ['GITLAB_TOKEN']}"}).raise_for_status()
    except requests.exceptions.HTTPError as exc:
      print("    failed to merge!", exc)
    time.sleep(0.5)
