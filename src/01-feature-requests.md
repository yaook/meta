# Feature Requests

## Scope

This document applies to all repositories in the [Yaook GitLab namespace](https://gitlab.com/yaook).

## Conventions used in this Document

The keywords "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119).

## Workflow

Feature requests, also called Feature Proposals, follow a defined workflow. This workflow may seem cumbersome, but the intent is to avoid waste of time caused by solutions which do not address non-obvious constraints or by work invested by different teams into the same solution without synchronization.

The goal of the process is to have, before implementation begins, a rough specification towards which developers can work. By having the specification created and reviewed collaboratively, the chances for non-obvious constraints causing issues are reduced. This is especially helpful for newcomers to the project, who cannot be expected to know all constraints of the project.

This process is OPTIONAL but highly RECOMMENDED for anything which goes beyond simple fixes. Otherwise, contributors risk that their contribution is rejected because it does not fit constraints the contributors were not aware of.

### Life cycle of a Feature Proposal

1. Creation of an Issue on GitLab

    First, an issue MUST be filed on GitLab. This issue SHOULD use the following template and MUST be labeled as `~"workflow:discussion"`:

    ```markdown
    /label ~"workflow::discussion"
    /assign me

    ## Summary

    <!-- Describe the proposed feature in a few sentences; the more detail, the better, but generally one or two sentences are sufficient if you provide a well laid-out use case -->

    ### Use Cases

    <!-- At least one specific use case needs to be provided. This should be described in as much detail as possible, making the outer constraints clear, so that others know where you're coming from -->

    ## Proposal

    <!-- If available, provide a proposal for an implementation. This should not be very detailed and just a rough sketch. -->

    ## Specification

    The keywords "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this section are to be interpreted as described in [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119).

    <!-- OPTIONAL: Write a proposed specification either as full text or a bullet points. -->
    ```

    Notes:

    - The contents of the "Specification" section are optional, we'll get to that later.
    - You may not have the necessary permissions to assign labels right away; someone else will do it for you then.
    - The request should be filed in the project it refers to. If your request spans multiple projects, try to find a place where it fits best. Worst case, someone will move it.

2. Discussion of the proposal and specifying it

    The proposal will now be discussed by other contributors. This process may raise issues with the proposal itself, voice support or extend the proposal in order to fit the project better.

    When the discussion is over, a specification in the form of either text or bullet points MUST be written and injected into the issue description by either the author of the proposal or by a project maintainer.

    The specification MUST be reviewed by a maintainer. Once the specification has passed review, the `~"workflow::specified"` label MUST be on the issue.

    If the feature proposal gets rejected, the workflow label MUST be retained as-is and the issue SHOULD be closed, unless further discussion is expected to happen in the near future. Feature Proposals MAY be reopened by entities with a concrete proposal to fix the reasons for rejection.

3. Implementation

    A contributor may at any time assign themselves to the feature proposal (or get assigned by a project maintainer on request) to signal that work will start. In that case, the issue is MUST be labelled with the `~"workflow::implementing"` label, even if no merge request is created right away.

    For the main development process, please see the [Developing](./01-developing.md) document.

    If during the development process the contributor cannot continue working on it, the labelling MUST be changed:

    - if external factors (upstream dependencies, other issues) block the progression of the implementation, the label MUST be changed to `~"workflow::blocked"`.
    - if the contributor cannot spend more time on the issue (e.g. it got deprioritised), the label MUST be changed to `~"workflow::specified"` in order to signal that others may take up the work.

    Notes:

    - There is not necessarily a 1:1 mapping between Feature Proposals and merge requests. Especially for complex features, it might make sense to have multiple smaller MRs to achieve the goal over one gigantonormeous MR.

4. Finalization

    Once the specification has been implemented (all corresponding MRs merged), a project maintainer MUST close the issue to signal its completion. The workflow label SHOULD be retained to see at a first glance whether an issue was (supposedly) completed or not.
