# META

This repo just contains meta scripts to manage all of our projects and groups.

## Subscribe to Meetings

The calendar is automatically pushed to: https://gitlab.com/yaook/meta/-/raw/devel/meetings/ical_output/yaook.ics

| Meeting     | Description                             |
| :---------- | :-------------------------------------- |
| Fortnightly | Project-scoped bi-weekly gathering.     |
| Shore Leave | yaook/k8s specific weekly gathering.    |

## project_settings.py

Can be used to automatically manage features, MR settings and approvals on
all of our projects.

## yaml2ics

The [yaml2ics](https://github.com/scientific-python/yaml2ics) tool is used for iCal generation.

```
# Generate iCal files from yaml descriptors
yaml2ics meetings/yaml_input/yaook.yaml > meetings/ical_output/yaook.ics
```
