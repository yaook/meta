#!/usr/bin/env python3
import typing
import gitlab
import os

from gitlab.v4.objects.groups import Group
from gitlab.v4.objects.projects import Project

# Enter Project IDs here if they should not be managed using this tooling
EXCLUDED_PROJECTS = []

MR_APPROVERS = {
    "QA-op": [
        44540,  # Jonas
        2899983,  # Felix
        3362864,  # Tobias A.
        7503825,  # Yaook Bot
        7941534,  # Maxim
        8252949,  # Stefan
        11486256,  # Robert
    ],
    "QA-k8s": [
        44540,  # Jonas
        1348504,  # Steve
        284080,  # Silvio
        19693679,  # Bruno
    ],
    "QA-bm": [
        44540,  # Jonas
        7296476,  # Sven
        14509281,  # Ilja
        7304802,  # Samuel
        7831402,  # Adrian
        10780366,  # Johannes
    ]
}

PROJECT_CLASS = {
    29738620: "k8s",  # k8s
    29777531: "k8s",  # images/k8s-ci
    52035944: "bm",   # bare-metal/apt-mirror
    27481923: "bm",   # bare-metal/disk-images
    28679112: "bm",   # metal-controller
    57621216: "bm",   # bare-metal/ansible-playbooks
}

PROJECT_CLASS_REQUIRED_RULES = {
    "k8s": ["QA-k8s"],
    "op": ["QA-op"],
    "bm": ["QA-bm"],
}

PROJECT_OVERRIDES = {
    29738620: {  # k8s
        "merge_trains_enabled": True,
        "builds_access_level" : "private", # Let's not leak any CI info
        "security_and_compliance_access_level" : "private"
    },
    21574229: {  # operator
        "merge_trains_enabled": True
    },
}

APPROVAL_OVERRIDES = {
    21574229: {  # operator
        "approvals_required": 2
    },
}


def get_all_projects(gl: gitlab.Gitlab, group: Group) -> typing.List[Project]:
    print("Found Group %s (%s)" % (group.name, group.id))
    p = group.projects.list(all=True)
    p = [gl.projects.get(x.id) for x in p]
    for subgroup in group.subgroups.list(all=True):
        sub = gl.groups.get(subgroup.id)
        p.extend(get_projects(gl, sub))
    return p


def get_projects(gl: gitlab.Gitlab, group: Group) -> typing.List[Project]:
    projects = get_all_projects(gl, group)
    projects = [p for p in projects if p.id not in EXCLUDED_PROJECTS]
    projects = [p for p in projects if not p.archived]
    return projects


def set_approvals(project: Project):
    class_ = PROJECT_CLASS.get(project.id, "op")
    required_rules = set(PROJECT_CLASS_REQUIRED_RULES[class_])
    rules = project.approvalrules.list()
    for rule in rules:
        if rule.name == 'All Members':
            rule.approvals_required = 0
            rule.save()
        elif rule.name in required_rules:
            rule.approvals_required = 1
            rule.user_ids = MR_APPROVERS[rule.name]
            rule.group_ids = []
            for name, value in APPROVAL_OVERRIDES.get(project.id, {}).items():
                setattr(rule, name, value)
            rule.save()
            required_rules.discard(rule.name)
        else:
            rule.delete()
    for rulename in required_rules:
        rule = {
            "name": rulename,
            "approvals_required": 1,
            "rule_type": "regular",
            "user_ids": MR_APPROVERS[rulename],
            "group_ids": [],
        }
        for name, value in APPROVAL_OVERRIDES.get(project.id, {}).items():
            rule[name] = value
        project.approvalrules.create(rule)


def manage_project(project: Project):
    print("Working on Project %s" % project.name)

    # Project visibility
    project.visibility = 'public'

    # General Project features
    project.auto_devops_enabled = False
    project.ci_job_token_scope_enabled = False
    project.container_registry_enabled = True
    project.issues_enabled = True
    project.jobs_enabled = True
    project.lfs_enabled = False
    project.merge_pipelines_enabled = True
    project.merge_requests_enabled = True
    project.merge_trains_enabled = False
    project.packages_enabled = True
    project.printing_merge_request_link_enabled = True
    project.request_access_enabled = True
    project.requirements_enabled = False
    project.security_and_compliance_enabled = False
    project.service_desk_enabled = False
    project.shared_runners_enabled = False
    project.snippets_enabled = False
    project.squash_option = 'never'
    project.wiki_enabled = True
    project.operations_access_level = 'disabled'

    # Merge Request
    project.merge_method = 'merge'
    project.merge_requests_access_level = 'enabled'
    project.only_allow_merge_if_all_discussions_are_resolved = True
    project.only_allow_merge_if_pipeline_succeeds = True

    # MR Approvals
    set_approvals(project)
    approval_manager = project.approvals.get()
    approval_manager.reset_approvals_on_push = True
    approval_manager.disable_overriding_approvers_per_merge_request = True
    approval_manager.merge_requests_author_approval = False
    approval_manager.save()

    project.save()

    # Apply per-project overrides, currently mainly used to enable merge trains
    # in yaook/k8s.
    # Note: We need to save the project a second time because it is not clear in which
    # order conflicting options (default vs. overrides) are evaluated.
    for name, value in PROJECT_OVERRIDES.get(project.id, {}).items():
        setattr(project, name, value)

    project.save()


if __name__ == '__main__':
    gl = gitlab.Gitlab(
        "https://gitlab.com",
        private_token=os.environ["GITLAB_TOKEN"])
    gl.auth()
    group_yaook = gl.groups.get(8953118)
    projects = get_projects(gl, group_yaook)
    print(f"Working on Projects: {[p.name for p in projects]}")
    for project in projects:
        manage_project(project)
